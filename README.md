# CarPricePredicition 
We have worked on a project called automobile price prediction using a supervised model.
In essence, it entails determining a car's price based on its numerous attributes.
With an accuracy rate of 83%, the decision tree machine learning approach was the most reliable one we developed.
Despite the presence of several independent variables, there is only one dependent variable whose actual and predicted values are compared to evaluate accuracy.


## Deployment
https://namgyalw-carpricepredicition-app-9qmctx.streamlit.app/

